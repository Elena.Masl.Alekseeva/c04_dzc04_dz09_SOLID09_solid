﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Otus_Dz9_Solid.IParam;

namespace Otus_Dz9_Solid
{
    /// <summary>
    /// Список параметров игры
    /// </summary>
    public interface IParams
    {
        public List<Param> ListParams { get; set; }
    }

    /// <summary>
    /// Требубется инициализация параметров 
    /// </summary>
    public interface IParamsNeedInit : IParams
    {
        public bool Init();
    }

    /// <summary>
    /// Инициализация параметров через консоль
    /// </summary>
    public abstract class AParamsInitFromconsol : IParamsNeedInit
    {
        public List<Param> ListParams { get; set; }

        public bool Init()
        {
            int? res = null;
            foreach (var pa in ListParams)
            {
                res = ConsolMethods.GetIntFromConsol(IParam.MakeQuestion(pa.ConsolName));
                if (res != null)
                    pa.ParamValue = (int)res;
                else
                    return false;
            }
            return true;
        }
    }

    /// <summary>
    /// Инициализация параметров через файл
    /// </summary>
    public abstract class AParamsInitFromFile : IParamsNeedInit
    {
        public List<Param> ListParams { get; set; }
        public bool Init()
        {
            /// here init From File
            return true;
        }
    }

    /// <summary>
    /// Вывод параметров на консоль
    /// </summary>
    public interface IParamsWriteToConsol : IParams
    {
        public void WriteToConsol()
        {
            {
                foreach (var pa in ListParams)
                {
                    ConsolMethods.WriteParam(pa.ConsolName, pa.ParamValue.ToString());
                }
            }
        }
    }

}
