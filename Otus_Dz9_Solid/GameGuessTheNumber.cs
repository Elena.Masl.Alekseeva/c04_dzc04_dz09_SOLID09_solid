﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Dz9_Solid
{
    public class GameGuessTheNumber : IGame//AGame
    {
        private IParams _iParam;
        
        /// <summary>
        /// Параметры игры
        /// </summary>
        public IParams Params { get { return _iParam; } set { _iParam = value; } }

        /// <summary>
        /// Число, которое мы отгадываем
        /// </summary>
        int? SearcheNumber = null;
        Random rand = new Random();

        public GameGuessTheNumber()
        {
            _iParam = new ParamsGuessTheNumber();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void PlayGame()
        {
            if (((IParamsNeedInit)Params).Init())
            {
                ((IParamsWriteToConsol)Params).WriteToConsol();
                SearcheNumber = rand.Next((int)((ParamsGuessTheNumber)Params).RangeMin, (int)((ParamsGuessTheNumber)Params).RangeMax);

                while (((ParamsGuessTheNumber)Params).CountRepeat > 0)
                {
                    int? intFeromConsol = ConsolMethods.GetIntFromConsol(string.Format("И так, у вас {0} попыток", ((ParamsGuessTheNumber)Params).CountRepeat));

                    if (intFeromConsol != null)
                    {
                        if (intFeromConsol > SearcheNumber)
                            Console.WriteLine("Меньше");

                        if (intFeromConsol < SearcheNumber)
                            Console.WriteLine("Больше");

                        if (intFeromConsol == SearcheNumber)
                        {
                            Console.WriteLine("Вы угадали! Поздравляем!!!");
                            break;
                        }

                        ((ParamsGuessTheNumber)Params).CountRepeat--;
                        if (((ParamsGuessTheNumber)Params).CountRepeat == 0)
                            Console.WriteLine("Ваши поппытки закончились, вы не угадали, очень жаль. Искомое число было " + SearcheNumber.ToString());
                    }
                }
            }
        }
    }
}

