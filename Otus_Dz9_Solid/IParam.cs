﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Dz9_Solid
{
    public interface IParam
    {
        /// <summary>
        /// Имя параметра
        /// </sum
        public string ParamName { get; set; }
        /// <summary>
        /// Значение параметра
        /// </summary>
        public object ParamValue { get; set; }

        public static string MakeQuestion(string str)
        {
            return "Задайте значение: " + str;
        }
    }
    
    
    
    /// <summary>
    /// Класс параметр игры
    /// </summary>
    public class Param : IParam
    {
        /// <summary>
        /// Вопрос, выводимый на консоль
        /// </summary>
        public string ConsolName;
        /// <summary>
        /// Имя параметра
        /// </summary>
        public string ParamName { get; set; }
        /// <summary>
        /// Значение параметра
        /// </summary>
        public object ParamValue { get; set; }

        public Param(string messageForInitInConsol, string parName, int paramValue)
        {
            ConsolName = messageForInitInConsol;
            ParamName = parName;
            ParamValue = paramValue;
        }


    }
}
