﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Dz9_Solid
{
    public interface IGame
    {
        public IParams Params { get; set; }
        public abstract void PlayGame();
    }
}
