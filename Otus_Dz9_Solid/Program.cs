﻿// See https://aka.ms/new-console-template for more information

using Otus_Dz9_Solid;

GameGuessTheNumber GameWePlayed = new GameGuessTheNumber();

Console.WriteLine("Начать игру? Y/N");

string? line = Console.ReadLine();

if (!string.IsNullOrEmpty(line) && line.ToLower() == "y")
{
    while (true)
    {
        GameWePlayed.PlayGame();
        
        Console.WriteLine("Хотите сыграть ещё раз? Y/N");
        line = Console.ReadLine();
        if (string.IsNullOrEmpty(line) || line.ToLower() != "y")
            break;
    }
}
