﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Dz9_Solid
{
    public static class ConsolMethods
    {
        
        public static int? GetIntFromConsol(string message = "")
        {
            Console.WriteLine("");
            while (true)
            {
                Console.WriteLine(message);

                int res;
                if (int.TryParse(Console.ReadLine(), out res))
                {
                    return res;
                }
                else
                {
                    Console.WriteLine("Неправильный формат целого числа");
                    Console.WriteLine("Вы хотите продолжить? Y/N");
                    string? line = Console.ReadLine();
                    if (string.IsNullOrEmpty(line) || line.ToLower() != "y")
                    {
                        break;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Вывод на консоль
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obgToStr"></param>
        public static void WriteParam(string name, string obgToStr)
        {
            Console.WriteLine(name + " " + obgToStr);
        }
    }
}
