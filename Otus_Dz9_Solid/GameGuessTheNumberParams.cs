﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Dz9_Solid
{
    /// <summary>
    /// Параметры игры "Угадай число"
    /// </summary>
    public class ParamsGuessTheNumber : AParamsInitFromconsol, IParamsWriteToConsol
    {
        public int CountRepeat
        {
            get 
            { 
                return (int)ListParams[0].ParamValue;
            }
            set
            {
                ListParams[0].ParamValue = value;
            }
        }
        public int RangeMin
        {
            get
            {
                return (int)ListParams[1].ParamValue;
            }
            set
            {
                ListParams[1].ParamValue = value;
            }
        }
        public int RangeMax
        {
            get
            {
                return (int)ListParams[2].ParamValue;
            }
            set
            {
                ListParams[2].ParamValue = value;
            }
        }

        public ParamsGuessTheNumber()
        {
            ListParams = new List<Param>();

            ListParams.Add(new Param("колличество попыток", "CountRepeat", 0));
            ListParams.Add(new Param("минимум диапазона для угадываемого числа", "RangeMin",  0));
            ListParams.Add(new Param("максимум диапазона для угадываемого числа", "RangeMax", 0));
        }
    }
}
